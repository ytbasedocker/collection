#!/bin/bash

imagename=x-base-debian
image=registry.gitlab.com/ytbasedocker/collection/${imagename}:latest

docker pull ${image}

export myUID=$(id -u)
export myGID=$(id -g)



startit(){
	myname=${imagename}
	docker run -d --rm						\
		--user $myUID:$myGID									\
		--env="DISPLAY"							\
		--workdir="/home/${USER}" \
		--volume="/home/${USER}/alternativHome/${myname}:/home/${USER}"	\
		--volume="/etc/group:/etc/group:ro"             	\
		--volume="/etc/passwd:/etc/passwd:ro"           	\
		--volume="/tmp/.X11-unix:/tmp/.X11-unix:rw"     	\
        --volume=/dev/shm:/dev/shm \
        --device=/dev/snd:/dev/snd \
		--name ${myname} \
		--hostname ${myname} \
		${image}
}

startit
