#!/bin/bash
set -o errexit -o nounset -o pipefail

devinit() {
    dockerimage=registry.gitlab.com/ytdocker/docker-python-bash
         mypath=$(dirname $0)
      basemount=$(realpath ${mypath})

    docker pull ${dockerimage}
    docker run -it --rm \
        -v ${basemount}:/workdir \
        --workdir /workdir \
        ${dockerimage} bash
}

run() {
              PACKAGES=ddclient
    CI_COMMIT_REF_SLUG=testdev
        CI_PIPELINE_ID=testdev
     CI_REGISTRY_IMAGE=testdev
    ./build.py $PACKAGES --tag $CI_COMMIT_REF_SLUG --tag PipeID$CI_PIPELINE_ID --regbasepath $CI_REGISTRY_IMAGE
}

if [ "" == "${1:-}" ]; then
  echo "gib verb ein"
  out=$(declare -F | awk '{ print substr($0,12) }' | grep -v "pause" | grep -v "findfreeport" | grep -v "port-forward")
  echo $out
  exit 1
fi


$1
